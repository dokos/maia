## Maia

Patient Record Management App for Midwifes

[![pipeline status](https://gitlab.com/dokos/maia/badges/main/pipeline.svg)](https://gitlab.com/dokos/maia/-/commits/main)
[![latest release](https://gitlab.com/dokos/maia/-/badges/release.svg)](https://gitlab.com/dokos/maia/-/releases)

#### License

AGPLv3
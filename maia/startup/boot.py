# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# See license.txt"


from __future__ import unicode_literals
import frappe
from frappe.utils import cint, now
from maia.maia_accounting.utils import get_fiscal_year

def boot_session(bootinfo):
	"""boot session - send website info if guest"""

	bootinfo.website_settings = frappe.get_doc('Website Settings')

	if frappe.session.user != 'Guest':
		bootinfo.practitioner = frappe.db.get_value("Professional Information Card", dict(user=frappe.session.user), "name")

		try:
			bootinfo.fiscal_year = get_fiscal_year(date=now(), practitioner=bootinfo.practitioner)
		except Exception as e:
			frappe.log_error(e)

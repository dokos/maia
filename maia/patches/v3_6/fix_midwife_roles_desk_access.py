import frappe

from frappe.core.doctype.role.role import desk_properties


def execute():
	for role in ["Midwife", "Midwife Substitute"]:
		role_doc = frappe.get_doc("Role", role.name)
		for key in desk_properties:
			role_doc.set(key, 1)
		role_doc.save()

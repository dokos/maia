import frappe

def execute():
	"""Set dynamic_template to True for all Web Pages, to prevent caching."""
	for page in frappe.get_all("Web Page", fields=["name"]):
		frappe.db.set_value("Web Page", page.name, "dynamic_template", 1)

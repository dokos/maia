import frappe

def execute():
	SEARCH = '<a class="btn btn-primary appointment" href="/login">'
	REPLACE = '<a class="btn btn-primary appointment" href="/appointment">'

	try:
		home_page = frappe.get_doc("Web Page", {"route": "home"})
		home_page.main_section_html = home_page.main_section_html.replace(SEARCH, REPLACE)
		home_page.save()
	except frappe.DoesNotExistError:
		pass

frappe.ready(() => {
	$(".hero-content .btn-primary").on("click", (e) => {
		e.preventDefault();
		accueillogin("lucile@dokos.io", "demomaia", "/app")
	});

	$(".hero-content .btn-primary-light").on("click", (e) => {
		e.preventDefault();
		accueillogin("isabelle@dokos.io", "demomaia", "/me")
	});

	 $(".navbar-nav .nav-link:contains('Mon compte')").on("click", (e) => {
		e.preventDefault();
		accueillogin("isabelle@dokos.io", "demomaia", "/me")
	});

	$(".for-login .email-field #login_email").val("isabelle@dokos.io")
	$(".for-login .password-field #login_password").val("demomaia")

	$(".for-login").append(
		"<div class='text-center sign-up-message'>Pour vous connecter aux espaces de travail de Maia, utilisez l'adresse <strong>lucile@dokos.io</strong></div>"
	)
});

const accueillogin = (username, password, target) => {
	frappe.call({
		"method": "login",
		args: {
			usr: username,
			pwd: password
		}
	}).then(r => {
		if(r.exc) {
			alert(__("Error, please contact help@dokos.io"));
		} else {
			console.log("Logged in");
			window.location.href = target;
		}
	})
}
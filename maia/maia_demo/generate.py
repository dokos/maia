import datetime
import random

import frappe
import frappe.utils
from frappe.model.document import Document

from maia.maia_demo.procgen_utils import generate_doc, get_random_docname, insert_fixtures

N_MIDWIVES = 3
N_INITIAL_PATIENTS = 10
NEW_PATIENT_PROBABILITY = 0.1

MIDWIFE_DT = "Professional Information Card"
MIDWIFE_SWITCH_PROBABILITY = 0.05

WEEKEND_PATIENT_PROBABILITY = 0.1

last_midwife_for_patient: dict[str, Document] = {}


def _repeated(fragment: str, n: int, probability: float = 1.0):
	return [{"fragment": fragment, "probability": probability} for _ in range(n)]


def generate_consultations_plan():
	return [
		"Gynecological Consultation",

		*_repeated("Free Consultation", 5, 0.5),

		"Gynecological Consultation",
		*_repeated("Gynecological Consultation", 3, 0.3),

		"Pregnancy Consultation",
		*_repeated("Gynecological Consultation", 3, 0.3),

		"Birth Preparation Consultation",
		"fragments/Group BP Consultation",
		"Birth Preparation Consultation",
		"fragments/Group BP Consultation",
		"Birth Preparation Consultation",

		{ "wait": "term" },
		"Prenatal Interview Consultation",
		"Early Postnatal Consultation",
		lambda x: "Early Postnatal Consultation" if x["term"] < 280 else "Postnatal Consultation",
		"Postnatal Consultation",

		"Perineum Rehabilitation Consultation",
		{ "wait": 200 },
	]

WAIT_TIMES = {
	"Consultation": 15,
	"Free Consultation": 25,
	"Pregnancy Consultation": 30,
	"Gynecological Consultation": 30,
	"Birth Preparation Consultation": 15,
	"Prenatal Interview Consultation": 7,
	"Perineum Rehabilitation Consultation": 7,
	"Early Postnatal Consultation": 7,
	"Postnatal Consultation": 7,
}


def step_generate_midwives():
	print(f"- midwives ({N_MIDWIVES})")
	for _ in range(N_MIDWIVES):
		midwife = generate_doc("fragments/Midwife")
		midwife.insert()
		print(f"  - {midwife.name}")


def util_set_midwife(patient: str | Document, midwife: str | Document):
	assert isinstance(patient, str) or isinstance(patient, Document)
	assert isinstance(midwife, str) or isinstance(midwife, Document)
	patient = patient.name if isinstance(patient, Document) else patient
	midwife = midwife.name if isinstance(midwife, Document) else midwife
	last_midwife_for_patient[patient] = midwife


def util_next_date(days: int, last_date: str | None = None):
	last_date = last_date or frappe.flags.current_date
	return frappe.utils.add_days(last_date, days)


def util_get_doc(doctype: str, docname: str | Document):
	assert isinstance(docname, str) or isinstance(docname, Document)
	if isinstance(docname, Document):
		assert docname.doctype == doctype
		return docname
	return frappe.get_doc(doctype, docname)


def util_pay_revenue(revenue_name: str, consult: Document):
	revenue = util_get_doc("Revenue", revenue_name)

	if revenue.status == "Paid":
		log(f"{revenue.name}: already paid", color="red")
		return

	pay = frappe.get_doc({
		"doctype": "Payment",
		"payment_date": frappe.flags.current_date,
		"payment_type": "Incoming payment",
		"party_type": "Patient Record",
		"party": consult.patient_record,
		"paid_amount": revenue.amount,
		"practitioner": consult.practitioner,
		"payment_method": "Virement [démo]",
		"payment_references": [
			{
				"doctype": "Payment References",
				"reference_type": "Revenue",
				"reference_name": revenue.name,
				"transaction_date": frappe.flags.current_date,
				"party": "CPAM",
				"outstanding_amount": revenue.amount,
				"patient_record": consult.patient_record,
				"paid_amount": revenue.amount,
			}
		],
	})
	pay.submit()

	# log(f"{revenue.name}: Paid", color="green")


def ansi_color(color: str):
	match color:
		case "red":
			return "\033[31m"
		case "green":
			return "\033[32m"
		case "yellow":
			return "\033[33m"
		case "blue":
			return "\033[34m"
		case "magenta":
			return "\033[35m"
		case "cyan":
			return "\033[36m"
		case "white":
			return "\033[37m"
		case "reset":
			return "\033[0m"
		case _:
			raise ValueError(f"invalid color: {color!r}")


def log(*args, color="", **kwargs):
	prefix = ""
	suffix = ""
	if color:
		prefix = ansi_color(color)
		suffix = ansi_color("reset")

	print(prefix + str(frappe.flags.current_date), *args, suffix, **kwargs)


def substep_generate_consultations_for_patient(patient: Document):
	initial_midwife = get_random_docname(MIDWIFE_DT)
	util_set_midwife(patient, initial_midwife)
	log(f"{patient.name}: initial midwife: {initial_midwife!r}", color="blue")

	n_pregnancies = random.randint(1, 3)
	i_pregnancy = 0
	while i_pregnancy < n_pregnancies:
		i_pregnancy += 1

		term = random.randint(250, 300)
		term_date = frappe.utils.add_days(frappe.flags.current_date, term)

		log(f"{patient.name}: pregnancy {i_pregnancy} of {n_pregnancies}, term: {term} days ({term_date})", color="green")

		consultations = generate_consultations_plan()

		remaining_consultations = list(consultations)
		while remaining_consultations:
			c = remaining_consultations.pop(0)

			midwife = last_midwife_for_patient[patient.name]

			if callable(c):
				c = c({
					"term": term,
					"term_date": term_date,
				})

			if isinstance(c, str):
				consultation_fragment = c
			elif isinstance(c, dict) and "wait" in c:
				if c["wait"] == "term":
					# Wait until the end of the pregnancy
					if frappe.flags.current_date < term_date:
						log(f"{patient.name}: waiting for term", color="yellow")
						remaining_consultations.insert(0, c)
						remaining_days = frappe.utils.date_diff(term_date, frappe.flags.current_date)
						yield remaining_days
						continue
					else:
						log(f"{patient.name}: term reached", color="yellow")
						continue
				elif isinstance(c["wait"], int):
					yield c["wait"]
				else:
					raise ValueError(f"invalid wait: {c['wait']!r}")
			elif isinstance(c, dict):
				consultation_fragment = c["fragment"]
				probability = c.get("probability", 1.0)
				if random.random() > probability:
					continue  # Skip this consultation
			else:
				raise TypeError(f"invalid consultation type: {type(c)}")

			if midwife != last_midwife_for_patient[patient.name]:
				log(f"{patient.name}: {consultation_fragment!r}, exceptionally with midwife {midwife!r}", color="yellow")
			else:
				log(f"{patient.name}: {consultation_fragment!r}")

			if "Group" not in consultation_fragment:
				# Generate a fake appointment, created one week before the consultation
				appointment_dt = frappe.utils.get_datetime(frappe.flags.current_date)
				appointment_dt += datetime.timedelta(hours=random.randint(8, 18), minutes=random.choice([0, 15, 30, 45]))

				log(f"-> Appointment at {appointment_dt}", color="cyan")
				appointment_type = "fragments/Appointment/Solo"

				# Mock frappe.flags.current_date to generate the appointment one week before
				original_date = frappe.flags.current_date
				frappe.flags.current_date = frappe.utils.add_days(frappe.flags.current_date, -7)
				appointment = generate_doc(appointment_type, patient_record=patient.name, practitioner=midwife, start_dt=appointment_dt)
				appointment.insert()
				frappe.flags.current_date = original_date


			# TODO: generate group appointments, but need a way to sync the appointment date with the consultation date, and to generate a slot before.

			# Generate a consultation
			consult = generate_doc(consultation_fragment, patient_record=patient.name, practitioner=midwife)

			if consult.meta.has_field("codifications"):
				is_sunday = frappe.utils.getdate(frappe.flags.current_date).weekday() == 6
				if is_sunday:
					consult.append("codifications", { "codification": "F" })

			if frappe.flags.maia_demo_no_submit:
				consult.insert()
			else:
				consult.submit()

				# Fetch the Revenues (patient + social security)
				revenues = frappe.get_all(
					"Revenue", filters={
						"revenue_type": ("in", ["Consultation", "Social Security"]),
						"consultation_type": consult.doctype,
						"consultation": consult.name,
						"status": "Unpaid",
					},
					pluck="name",
				)

				for revenue in revenues:
					util_pay_revenue(revenue, consult)

			wait_time = WAIT_TIMES.get(consult.doctype, 1)
			yield wait_time

			if remaining_consultations and random.random() < MIDWIFE_SWITCH_PROBABILITY:
				midwife = get_random_docname(MIDWIFE_DT)
				if midwife != last_midwife_for_patient[patient.name]:
					log(f"{patient.name}: switching midwife for {midwife!r}", color="blue")
					util_set_midwife(patient, midwife)


def step_generate_patients(max_date):
	frappe.flags.maia_demo_no_submit = False

	patients = []
	consult_generators = []

	def _new_patient():
		patient = generate_doc("fragments/Patient")
		try:
			patient.insert()
		except frappe.exceptions.DuplicateEntryError:
			log("! duplicate patient, skipping", color="red")
			return

		log(f"{patient.name}: new patient", color="magenta")
		patients.append(patient)
		consult_generators.append(
			{
				"delay": random.randint(1, 120 / 2),  # 60 days
				"generator": substep_generate_consultations_for_patient(patient),
			}
		)

	print(f"- initial patients ({N_INITIAL_PATIENTS})")
	for _ in range(N_INITIAL_PATIENTS):
		_new_patient()

	print(f"- consultations")

	while frappe.flags.current_date < max_date:
		step = random.randint(1, 3)
		frappe.flags.current_date = util_next_date(days=step)

		weekday = frappe.utils.getdate(frappe.flags.current_date).weekday()
		if weekday in (5, 6) and random.random() >= WEEKEND_PATIENT_PROBABILITY:
			continue

		if random.random() < NEW_PATIENT_PROBABILITY:
			_new_patient()

		for i, g in enumerate(consult_generators):
			if g["delay"] > 0:
				g["delay"] -= step
				continue
			try:
				d = next(g["generator"])
				if d is not None:
					g["delay"] = d
			except StopIteration:
				consult_generators.pop(i)
				break

	# Generate draft consultations
	print(f"- draft consultations for the future")
	frappe.flags.maia_demo_no_submit = True
	for g in consult_generators:
		frappe.flags.current_date = frappe.utils.add_days(max_date, random.randint(1, 20))
		try:
			next(g["generator"])
		except StopIteration:
			pass

	print("Summary:")
	print(f"  {len(patients)} patients")
	print(f"  {len(consult_generators)} active generators remaining")


def generate(days=365):
	print(f"Generating data for {days} days...")

	end_date = frappe.utils.getdate(frappe.utils.nowdate())
	start_date = frappe.utils.add_days(end_date, -days)

	print(f"  from {start_date} to {end_date}")

	frappe.flags.current_date = start_date

	insert_fixtures()
	step_generate_midwives()
	step_generate_patients(max_date=end_date)

	# Note: A commit is done after each payment.
	frappe.db.commit()

# Copyright (c) 2019, Dokos and Contributors
# See license.txt

from __future__ import unicode_literals
import frappe
from maia.maia_demo.setup import MaiaSetup

from maia.maia_demo.generate import generate

def make(days = 365, main_password = "demomaia"):
	set_flags()
	setup_wizard(main_password)
	frappe.db.commit()

	site = frappe.local.site
	frappe.destroy()
	frappe.init(site)
	frappe.connect()

	set_flags()
	MaiaSetup().run()
	frappe.db.commit()
	frappe.clear_cache()

	resume(days)


def resume(days):
	set_flags()
	generate(days=days)


def set_flags():
	frappe.flags.mute_emails = True
	frappe.flags.mute_gateways = True
	frappe.flags.mute_notifications = True
	frappe.flags.in_demo = 1


def setup_wizard(main_password = "demomaia"):
	print("Setup Wizard...")
	from frappe.desk.page.setup_wizard.setup_wizard import setup_complete
	completed = setup_complete(
		{
			"full_name": "Lucile Asselin",
			"email": "lucile@dokos.io",
			"password": main_password,
			"currency": "EUR",
			"timezone": "Europe/Paris",
			"country": "France",
			"language": "fr"
		}
	)
	print("Setup Wizard Completed", completed)

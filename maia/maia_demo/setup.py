# Copyright (c) 2023, Dokos and Contributors
# See license.txt

import frappe

DEFAULT_SCHEDULE = [
	{
		"day": "Monday",
		"start_time": "09:00",
		"end_time": "12:00"
	},
	{
		"day": "Monday",
		"start_time": "13:00",
		"end_time": "18:00"
	},
	{
		"day": "Tuesday",
		"start_time": "09:00",
		"end_time": "12:00"
	},
	{
		"day": "Tuesday",
		"start_time": "13:00",
		"end_time": "18:00"
	},
	{
		"day": "Thursday",
		"start_time": "09:00",
		"end_time": "12:00"
	},
	{
		"day": "Thursday",
		"start_time": "13:00",
		"end_time": "18:00"
	},
	{
		"day": "Friday",
		"start_time": "09:00",
		"end_time": "12:00"
	},
	{
		"day": "Friday",
		"start_time": "13:00",
		"end_time": "18:00"
	}
]

class MaiaSetup:
	def run(self):
		self.setup_users()
		self.setup_web_page()
		self.setup_website_settings()
		self.setup_theme_script()

	def setup_users(self):
		print("Setup Users...")
		from frappe.utils.password import update_password

		lucile = frappe.get_doc("User", "lucile@dokos.io")
		lucile.user_image = "/assets/maia/images/demo/lucile.jpg"
		lucile.add_roles("Midwife", "Patient", "Midwife Substitute", "Appointment User")
		lucile.save()

		pic = frappe.new_doc("Professional Information Card")
		pic.email = "lucile@dokos.io"
		pic.last_name = "Lucile"
		pic.first_name = "Asselin"
		pic.user = "lucile@dokos.io"
		pic.allow_online_booking = 1
		for schedule in DEFAULT_SCHEDULE:
			pic.append("consulting_schedule", schedule) 
		pic.insert()

		patient = frappe.get_doc(
			{
				"email": "isabelle@dokos.io",
				"first_name": "Isabelle",
				"last_name": "Dupuy",
				"language": "fr",
				"doctype": "User",
				"send_welcome_email": 0,
				"enabled": 1,
				"user_image": "/assets/maia/images/demo/isabelle.jpg",
			}
		)
		patient.flags.create_contact_immediately = True
		patient.user_type = "Website User"
		patient.insert(ignore_if_duplicate=True)

		patient.reload()
		patient.add_roles("Patient")
		update_password(patient.name, "demomaia")

	def setup_web_page(self):
		with open(frappe.get_app_path("maia", "maia_demo", "data", "demo_web_page.json")) as f:
			docs = frappe.parse_json(f.read())

			for doc in docs:
				frappe.get_doc(doc).insert()

	def setup_website_settings(self):
		print("Website Settings Setup...")
		website_settings = frappe.get_single("Website Settings")
		website_settings.navbar_search = 0
		website_settings.hide_footer_signup = 1
		website_settings.hide_login = 1
		website_settings.banner_image = "/images/maia_squirrel.svg"
		website_settings.brand_html = "<img style='width: 40px;' src='/assets/maia/images/maia_squirrel.svg'>"
		website_settings.copyright = "Association Toobib"
		website_settings.home_page = "demo"
		website_settings.website_theme = "Maia"
		website_settings.top_bar_items = []
		website_settings.address = '<div class="ql-editor read-mode"><p>Démo Maia &middot; Maia est un logiciel libre de gestion de cabinet de sage-femme. Le projet est disponible ici : <a href="https://gitlab.com/dokos/maia">gitlab.com/dokos/maia</a></p><p>Crédits photos: Unsplash.com</p></div>'
		website_settings.banner_html = """
		<div class="text-center text-white" style="background-color: #4081ff;">
			<div>Maia rejoint le projet de création d'un dossier patient libre et open-source <strong>ToobibPro</strong></div>
			<div><a href="https://interhop.org/2023/06/06/fabrique-des-santes" class="font-weight-bold text-white" target="_blank">Cliquez ici pour en savoir plus</a></div>
		</div>
		"""
		website_settings.save()


	def setup_theme_script(self):
		with open(frappe.get_app_path("maia", "maia_demo", "data", "demo_web_script.js")) as f:
			script = f.read()
			frappe.db.set_value("Website Theme", "Maia", "js", script)

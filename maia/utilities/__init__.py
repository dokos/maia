import datetime
import frappe
import math
from frappe.utils import getdate, cint, global_date_format
import dateparser


def difference_in_weeks(start_date, end_date):
	dt_diff = getdate(end_date) - getdate(start_date)
	dt_diff_seconds = dt_diff.days * 86400.0 + dt_diff.seconds
	dt_diff_days = math.floor(dt_diff_seconds / 86400.0)
	weeks_diff = cint(math.ceil(dt_diff_days / 7.0))

	return weeks_diff


def parse_and_format_date(date):
	if isinstance(date, str):
		parsed = dateparser.parse(date)
		if parsed:
			return global_date_format(parsed)
	if isinstance(date, (datetime.datetime, datetime.date)):
		return global_date_format(date)
	return date

# Copyright (c) 2018, DOKOS and Contributors
# See license.txt

import frappe

def set_default_settings():
	system_settings = frappe.get_doc("System Settings")
	system_settings.update({
		"country": "France",
		"language": "fr",
		"time_zone": "Europe/Paris",
		"first_day_of_the_week": "Monday",
		"strip_exif_metadata_from_uploaded_images": 0,
	})
	system_settings.flags.ignore_permissions = True
	system_settings.save()


	website_settings = frappe.get_single("Website Settings")
	website_settings.app_name = "Maia"
	website_settings.app_logo = "/assets/maia/images/maia_squirrel.svg"
	# website_settings.website_theme = "Maia"
	website_settings.save()

	frappe.db.commit()

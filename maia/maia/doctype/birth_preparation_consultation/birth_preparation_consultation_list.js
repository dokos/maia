frappe.listview_settings['Birth Preparation Consultation'] = {
	add_fields: ["patient_name", "patient_record", "pregnancy_folder", "prenatal_interview_folder"],
	onload(listview) {
		listview.filter_area.add(listview.doctype, "practitioner", "=", frappe.boot.practitioner || "");
	},
};

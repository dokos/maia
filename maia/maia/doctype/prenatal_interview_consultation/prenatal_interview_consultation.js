// Copyright (c) 2017, DOKOS and contributors
// For license information, please see license.txt
frappe.provide('maia');

{% include "maia/public/js/controllers/consultations.js" %}

frappe.ui.form.on('Prenatal Interview Consultation', {
	onload(frm) {
		frm.fields_dict['prenatal_interview_folder'].get_query = function (doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
	}
});

maia.PrenatalInterviewConsultationController = class PrenatalInterviewConsultationController extends maia.BaseConsultationController {
};

$.extend(cur_frm.cscript, new maia.PrenatalInterviewConsultationController({ frm: cur_frm }));

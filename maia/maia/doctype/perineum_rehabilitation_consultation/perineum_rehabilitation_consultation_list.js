frappe.listview_settings['Perineum Rehabilitation Consultation'] = {
	add_fields: ["patient_name", "patient_record", "perineum_rehabilitation_folder", "pregnancy_folder"],
	onload(listview) {
		listview.filter_area.add(listview.doctype, "practitioner", "=", frappe.boot.practitioner || "");
	},
};

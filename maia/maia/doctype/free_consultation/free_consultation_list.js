frappe.listview_settings['Free Consultation'] = {
	add_fields: ["patient_name", "patient_record", "gynecological_folder", "perineum_rehabilitation_folder"],
	onload(listview) {
		listview.filter_area.add(listview.doctype, "practitioner", "=", frappe.boot.practitioner || "");
	},
};

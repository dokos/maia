// Copyright (c) 2019, DOKOS and contributors
// For license information, please see license.txt

/** Always call this method, it needs to know if the field is empty */
function has_form_value_changed(frm, fieldname) {
	const reset = () => {
		frm._prev = {
			__docname: frm.doc?.name,
			__doctype: frm.doc?.doctype,
		};
	};

	if (!frm._prev) {
		reset(); // first call
	}
	if (frm._prev.__docname !== frm.doc?.name || frm._prev.__doctype !== frm.doc?.doctype) {
		reset(); // document changed
	}
	if (frm._prev[fieldname] === frm.doc[fieldname]) {
		return false;
	}

	frm._prev[fieldname] = frm.doc[fieldname] || null;
	return true;
}

frappe.ui.form.on(cur_frm.doctype, {
	onload: function (frm) {
		if (frm.doc.__islocal) {
			let args;
			if (frm.doc.practitioner) {
				args = { practitioner: frm.doc.practitioner };
			} else {
				args = { user: frappe.session.user };
			}
			frappe.call({
				method: "maia.maia.utils.get_letter_head",
				args: args,
				callback(r) {
					frm.set_value("letter_head", r.message);
				}
			});
		}
	},

	refresh(frm) {
		has_form_value_changed(frm, "lab_exam_template");
		has_form_value_changed(frm, "drug_list_template");
	},

	lab_exam_template(frm) {
		const changed = has_form_value_changed(frm, "lab_exam_template");
		if (frm.doc.lab_exam_template && changed) {
			frappe.call({
				"method": "maia.maia.doctype.lab_exam_template.lab_exam_template.get_lab_exam_template",
				args: {
					lab_exam_template: frm.doc.lab_exam_template
				},
				callback: function (data) {
					$.each(data.message || [], function (i, v) {
						let d = frappe.model.add_child(frm.doc, "Lab Exam Prescription", "lab_prescription_table");
						d.lab_exam = v.exam_type;
						d.additional_notes = v.additional_notes;
					});
					refresh_field("lab_prescription_table");
				}
			});
		}
	},

	drug_list_template(frm) {
		const changed = has_form_value_changed(frm, "drug_list_template");
		if (frm.doc.drug_list_template && changed) {
			frappe.call({
				"method": "maia.maia.doctype.drug_list_template.drug_list_template.get_drug_list_template",
				args: {
					drug_list_template: frm.doc.drug_list_template
				},
				callback: function (data) {
					$.each(data.message || [], function (i, v) {
						let d = frappe.model.add_child(frm.doc, "Drug Prescription", "drug_prescription_table");
						d.drug = v.drug;
						d.posology = v.posology;
						d.pharmaceutical_form = v.pharmaceutical_form;
						d.treatment_duration = v.treatment_duration;
						d.additional_notes = v.additional_notes;
					});
					refresh_field("drug_prescription_table");
				}
			});
		}
	}
});

// eslint-disable-next-line no-unused-vars
function print_drug_prescription(frm) {
	var w = window.open(
		frappe.urllib.get_full_url("/api/method/frappe.utils.print_format.download_pdf?" +
			"doctype=" + encodeURIComponent(frm.doc.doctype) +
			"&name=" + encodeURIComponent(frm.doc.name) +
			"&format=Drug Prescription" +
			"&no_letterhead=0" +
			"&_lang=fr")
	);
	if (!w) {
		frappe.msgprint(__("Please enable pop-ups"));
		return;
	}
}

// eslint-disable-next-line no-unused-vars
function print_lab_prescription(frm) {
	var w = window.open(
		frappe.urllib.get_full_url("/api/method/frappe.utils.print_format.download_pdf?" +
			"doctype=" + encodeURIComponent(frm.doc.doctype) +
			"&name=" + encodeURIComponent(frm.doc.name) +
			"&format=Lab Prescription" +
			"&no_letterhead=0" +
			"&_lang=fr")
	);
	if (!w) {
		frappe.msgprint(__("Please enable pop-ups"));
		return;
	}
}

// eslint-disable-next-line no-unused-vars
function print_echo_prescription(frm) {
	var w = window.open(
		frappe.urllib.get_full_url("/api/method/frappe.utils.print_format.download_pdf?" +
			"doctype=" + encodeURIComponent(frm.doc.doctype) +
			"&name=" + encodeURIComponent(frm.doc.name) +
			"&format=Echography Prescription" +
			"&no_letterhead=0" +
			"&_lang=fr")
	);
	if (!w) {
		frappe.msgprint(__("Please enable pop-ups"));
		return;
	}
}
